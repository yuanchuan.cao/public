#!bin/sh
read -p "hostname: " hostname
sudo hostname $hostname

echo "install brew"
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
brew update
echo "install brew finished"

echo "for command like realpath dirname"
brew install coreutils

brew install git
brew install ggrep
brew install gawk
brew install gsed

echo "now you can modify public_dir to a new address in the local software.config as you wish"


#!/bin/sh
script_relative_dir=../../../scripts/
. $(dirname $0)/${script_relative_dir}function.sh
get_env ${script_relative_dir}
# (bashInit "../")

osascript -e 'tell application "System Events" to tell every desktop to set picture to "$file_dir/wallpaper.png"'

pause_for_key

config_get public_dir
mkdir -p $public_dir
git clone https://gitlab.com/shima_rin/public $public_dir/public || echo "public repo already exists"
sh $public_dir/public/installation/software/install.sh
sh $public_dir/public/installation/tool/restore.sh


