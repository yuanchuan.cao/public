#!/bin/sh
script_relative_dir=../../scripts/
source $(dirname $0)/${script_relative_dir}function.sh
get_env ${script_relative_dir}
# (bashInit "../")

config_get backup_dir

rm -rf $backup_dir
mkdir -p $backup_dir

cp -f ~/.emacs $backup_dir/.emacs || echo ".emacs not found"
cp -f ~/.dict $backup_dir/.dict || echo ".dict not found"
cp -f $file_dir/../system/$machine/config/software.config $backup_dir/
cp -f $file_dir/../system/$machine/install.sh $backup_dir/
cp -f $file_dir/restore.sh $backup_dir/

config_get fav_song_playlist
cp -f $fav_song_playlist $backup_dir/fav_song_playlist

config_get neofetch_picture
cp -f $neofetch_picture $backup_dir/neofetch_picture

cp -rf ~/.ssh $backup_dir/.ssh

