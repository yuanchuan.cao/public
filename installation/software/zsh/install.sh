#!/bin/sh
script_relative_dir=../../../scripts/
source $(dirname $0)/${script_relative_dir}function.sh
get_env ${script_relative_dir}
# (bashInit "../")

if_install
echo install zsh...
if_mac
brew install zsh
brew install bash-completion@2  
sudo chsh -s /user/local/bin/zsh
fi

if_linux
sudo apt install -y zsh
sudo chsh -s $(which zsh)
fi

printf "2\n" | zsh | echo "init zsh config..."

echo install oh-my-zsh...
sh -c "$(curl -fsSL https://gitee.com/pocmon/ohmyzsh/raw/master/tools/install.sh)"

echo install zsh_z...
git clone https://github.com/agkozak/zsh-z $ZSH_CUSTOM/plugins/zsh-z

echo "install zsh-autosuggestions"
echo "ctl-f to complete all / alt-f to complete word"
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM}/plugins/zsh-autosuggestions

insert_or_replace_line \
    "plugins=(git" \
    "plugins=(git zsh-z zsh-autosuggestions)" \
    $zsh_path
fi


config_get completion_path


insert_or_replace_line \
    " bash_completion for zsh"  \
    "sh /usr/local/etc/profile.d/bash_completion.sh # bash_completion for zsh" \
    $zsh_path

insert_or_replace_line \
    " source completion"\
    " source $completion_path   # source completion "\
    $zsh_path

config_get zsh_theme
sga -m 100 -c "ZSH_THEME=\"" -r "ZSH_THEME=\"$zsh_theme\"" $zsh_path
echo zsh_theme changed to $zsh_theme...
