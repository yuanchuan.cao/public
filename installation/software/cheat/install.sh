#!/bin/sh
script_relative_dir=../../../scripts/
. $(dirname $0)/${script_relative_dir}function.sh
get_env ${script_relative_dir}
# (bashInit "./")

if_install
if_mac
brew install cheat
brew install fzf
fi

if_linux
sudo apt install -y cheat
sudo apt install -y fzf
fi

echo  redirect cheat sheet to locals
mkdir -p ~/.config/cheat && cheat --init > ~/.config/cheat/conf.yml

gsed -i '/editor: vi/c\editor: emacs -nw' ~/.config/cheat/conf.yml
gsed -i  "s+.*cheatsheets/community+    path: $file_dir/cheatsheets/community+" ~/.config/cheat/conf.yml
gsed -i  "s+.*cheatsheets/personal+    path: $file_dir/cheatsheets/personal+" ~/.config/cheat/conf.yml

fi

config_get completion_path
insert_or_replace_line \
    "cheat.zsh"\
    ". $file_dir/cheat.zsh" \
    $completion_path
