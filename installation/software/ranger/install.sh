#!/bin/sh
script_relative_dir=../../../scripts/
. $(dirname $0)/${script_relative_dir}function.sh
get_env ${script_relative_dir}
# (bashInit "../")

if_install
if_mac
brew install ranger
fi

if_linux
sudo apt install -y ranger
fi
fi

pip3 install pillow ranger-fm

mkdir -p ~/.config/ranger

insert_or_replace_line \
    "set preview_images " \
    "set preview_images true" \
    ~/.config/ranger/rc.conf

insert_or_replace_line \
    "set preview_images_method " \
    "set preview_images_method kitty" \
    ~/.config/ranger/rc.conf

insert_or_replace_line \
    "alias dir=" \
    "alias dir=\"ranger\"" \
    $zsh_path

insert_or_replace_line \
    "alias dir=" \
    "alias dir=\"ranger\"" \
    $bash_path
