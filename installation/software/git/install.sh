#!/bin/sh
script_relative_dir=../../../scripts/
. $(dirname $0)/${script_relative_dir}function.sh
get_env ${script_relative_dir}
# (bashInit "../")

if_install

echo install git...

if_mac
brew install git
brew install icdiff
brew install mdv
fi

if_linux
sudo apt install -y git
sudo apt install -y icdiff
pip3 install mdv
fi

user=$(whoami)
domain=$(hostname)

git config --global core.pager '`test "$TERM" = "dumb" && echo cat || echo less`'
git config --global alias.diff icdiff
git config --global icdiff.options '--highlight --line-numbers'
git config --global color.ui true
git config --global core.editor emacs
git config --global user.name "$user"
git config --global user.email "$user@$domain"

insert_or_replace_line \
    "alias diff=" \
    "alias diff=\"icdiff\"" \
    $zsh_path

insert_or_replace_line \
    "alias diff=" \
    "alias diff=\"icdiff\"" \
    $bash_path

fi
