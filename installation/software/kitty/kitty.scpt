-- install it by sys install-script kitty MyKitty
-- "cd to Downloads/icon, pick an icon and put into Downloads/icon/icon/icon.png" 
-- "at Downloads/icon and run sys icon, get the icns"
-- "open the app in ~/Application, show package contents and drag icns to resources"
-- "open privatcy, assitantive, +, add this app to the list"
-- "cmd+ spc, search MyKitty, drag to the dock"
-- "sys install-script will update"
-- "need privacy support for kitty"

on run
tell application "Finder"
set  screen_resolution to bounds of window of desktop
end tell
tell application "System Events" to ¬
tell application process "Dock" to ¬
set {dockWidth, dockHeight} to ¬
the size of list 1

set xPos to item 3 of screen_resolution * 2 / 3
set yPos to 0
set height to item 4 of screen_resolution * 2 / 5

tell application "Safari"
make new document
set URL of document 1 to "https://www.bilibili.com"
set bounds of window 1 to {xPos, yPos, item 3 of screen_resolution, height}
activate
make new document
set URL of document 1 to "https://www.google.com"
set bounds of window 1 to {xPos, height + 20, item 3 of screen_resolution, item 4 of screen_resolution - dockHeight}
activate
end tell

tell application "kitty"
     activate
end tell	

tell application "System Events" to tell application process "kitty"
     set position of window 1 to {0, 0}
     set size of window 1 to {xPos, item 4 of screen_resolution - dockHeight}
end tell

tell application "kitty"
  tell application "System Events"
        keystroke "emacs -nw"
	key code 36
        keystroke "o" using {control down, shift down}
	keystroke "tawawa"
	key code 36
        keystroke "o" using {control down, shift down}
	keystroke "neofetch"
	key code 36
        keystroke "o" using {control down, shift down}
  end tell
end tell

end run
