#!/bin/sh
script_relative_dir=../../../scripts/
. $(dirname $0)/${script_relative_dir}function.sh
get_env ${script_relative_dir}
# (bashInit "../")

if_mac
brew install gnupg
fi
install_simple pass

echo "get git repo for pass store: git@domain:name/repo" 
read -p "pass git domain: " domain
read -p "repo owner name: " name
read -p "repo name: " repo

git clone git@${domain}:${name}/${repo} ~/.password-store 

echo "put the key in ~/.ssh"
pause_for_key

gpg --import ~/.ssh/mastersub.key
echo "the key name can be very vague, for instance name after public key "
read -p "key id" keyid
pass init "$keyid"

