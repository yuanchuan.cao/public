#!/bin/sh
script_relative_dir=../../../scripts/
source $(dirname $0)/${script_relative_dir}function.sh
get_env ${script_relative_dir}
# (bashInit "./")

config_get restore_dir

[ -d ~/.emacs.d/cnfonts ] || emacs --eval '(progn (find-file "$file_dir/../../../lisp/emacs_install_package.el") (eval-buffer))'

#echo alias manullay to ~/.emacs.d/eshell/alias

#alias p sh /home/stan/Code/public/scripts/projectile.sh $1 $2 $3 $4 $5
#alias text bash /home/stan/Code/public/scripts/text.sh $1 $2 $3 $4 $5 $6 $7 $8 $9 $10 $11 $12 $13 $14
# alias tk tmux kill-session -t 

cp $restore_dir/.dict ~/.dict    || echo "can not found backup dict"
cp $restore_dir/.emacs ~/.emacs  || echo "can not found backup .emacs"


insert_or_replace_line \
    "alias emacs=" \
    "alias emacs=\"emacs -nw\"" \
    $zsh_path
insert_or_replace_line \
    "alias emacs=" \
    "alias emacs=\"emacs -nw\"" \
    $bash_path    

