#!/bin/sh
script_relative_dir=../../../scripts/
. $(dirname $0)/${script_relative_dir}function.sh
get_env ${script_relative_dir}
# (bashInit "../")

if_install

if_mac
brew install neofetch
fi

if_linux
sudo apt install -y neoftech
fi

fi

pict_path=$(sga -m 1 -M 1 -b "neofetch_picture" $config_path)

insert_or_replace_line \
    "alias neofetch=" \
    "alias neofetch=\"neofetch --kitty $pict_path\"" \
    $zsh_path

insert_or_replace_line \
    "alias neofetch=" \
    "alias neofetch=\"neofetch --kitty $pict_path\"" \
    $bash_path
