#!/bin/sh
script_relative_dir=../../../scripts/
. $(dirname $0)/${script_relative_dir}function.sh
get_env ${script_relative_dir}
# (bashInit "../")

install_glances () {
    install_simple glances
    insert_or_replace_line \
	"alias monitor=" \
	"alias monitor=\"glances\"" \
	$zsh_path
    insert_or_replace_line \
	"alias monitor=" \
	"alias monitor=\"glances\"" \
	$bash_path
}

install_cppman () {
    install_simple cppman
}

install_multitail () {
    install_simple multitail
}

install_proxychains4 () {
    install_simple proxychains4
    insert_or_replace_line \
	"alias proxy=" \
	"alias proxy=\"proxychains4 -q\"" \
	$zsh_path
    insert_or_replace_line \
	"alias proxy=" \
	"alias proxy=\"proxychains4 -q\"" \
	$bash_path    
}

install_copy () {
    if_linux
    sudo apt -y install xclip
fi
}

install_curl () {
    install_simple curl
}

install_tmux () {
    install_simple tmux
    insert_or_replace_line \
	"alias tl=" \
	"alias tl=\"tmux list-sessions\"" \
	$zsh_path
    insert_or_replace_line \
	"alias tl=" \
	"alias tl=\"tmux list-sessions\"" \
	$bash_path

    insert_or_replace_line \
	"alias ta=" \
	"alias ta=\"tmux a -t \"" \
	$zsh_path
    insert_or_replace_line \
	"alias ta=" \
	"alias ta=\"tmux a -t \"" \
	$bash_path

    insert_or_replace_line \
	"alias tk=" \
	"alias tk=\"tmux kill-session -t \"" \
	$zsh_path
    insert_or_replace_line \
	"alias tk=" \
	"alias tk=\"tmux kill-session -t \"" \
	$bash_path

    insert_or_replace_line \
	"alias tn=" \
	"alias tn=\"tmux new -d -s \"" \
	$zsh_path
    insert_or_replace_line \
	"alias tn=" \
	"alias tn=\"tmux new -d -s \"" \
	$bash_path
}

install_ip () {
    config_get ip_lookup_url
    insert_or_replace_line \
	"alias ip=" \
	"alias ip=\"curl $ip_lookup_url\"" \
	$zsh_path
    insert_or_replace_line \
	"alias ip=" \
	"alias ip=\"curl $ip_lookup_url\"" \
	$bash_path
}

populate_scripts () {
    insert_or_replace_line \
	"alias h3g=" \
	"alias h3g=\"sh $script_dir/git.sh \"" \
	$zsh_path
    insert_or_replace_line \
	"alias h3g=" \
	"alias h3g=\"sh $script_dir/git.sh \"" \
	$bash_path

    insert_or_replace_line \
	"alias h3p=" \
	"alias h3p=\"sh $script_dir/projectile.sh \"" \
	$zsh_path
    insert_or_replace_line \
	"alias h3p=" \
	"alias h3p=\"sh $script_dir/projectile.sh \"" \
	$bash_path

    insert_or_replace_line \
	"alias sga=" \
	"alias sga=\"sh $script_dir/sga.sh \"" \
	$zsh_path
    insert_or_replace_line \
	"alias sga=" \
	"alias sga=\"sh $script_dir/sga.sh \"" \
	$bash_path

    insert_or_replace_line \
	"alias x=" \
	"alias x=\"sh $script_dir/x.sh \"" \
	$zsh_path
    insert_or_replace_line \
	"alias sys=" \
	"alias sys=\"sh $script_dir/system.sh \"" \
	$bash_path

    insert_or_replace_line \
	"alias tawawa=" \
	"alias tawawa=\"sh $script_dir/tawawa.sh \"" \
	$zsh_path
    insert_or_replace_line \
	"alias tawawa=" \
	"alias tawawa=\"sh $script_dir/tawawa.sh \"" \
	$bash_path

    chmod a+x $script_dir/tawawa.sh
    chmod a+x $script_dir/system.sh
    chmod a+x $script_dir/sga.sh
    chmod a+x $script_dir/git.sh
    chmod a+x $script_dir/projectile.sh

}

install_resolution () {
    if_linux

    insert_or_replace_line \
	"alias resolution=" \
	"alias resolution=\" xrandr --output Virtual-1 --mode  \" #xrandr -q find device && resolution \"1920x1080\" " \
	$zsh_path
    insert_or_replace_line \
	"alias resolution=" \
	"alias resolution=\" xrandr --output Virtual-1 --mode  \" #xrandr -q find device && resolution \"1920x1080\" " \
	$bash_path
    fi

}

install_clear () {
    insert_or_replace_line \
	"alias clear=" \
	"alias clear=\"clear && printf '\e[3J'\"" \
	$zsh_path
    insert_or_replace_line \
	"alias clear=" \
	"alias clear=\"clear && printf '\e[3J'\"" \
	$bash_path
}


