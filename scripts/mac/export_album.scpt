on run argv
   set rootName to (item 1 of argv) as string
   set t1Name to (item 2 of argv) as string
   set tName to (item 3 of argv) as string
--set rootName to "/Users/yuanchuan/Downloads/imgcat_tmp/"

set tFolder to rootName & tName
tell me
     do shell script "rm -rf " & quoted form of tFolder
     do shell script "mkdir -p " & quoted form of tFolder
end tell
set tFolder to (rootName as POSIX file as text)  & tName
tell application "Photos"
       export (get media items of album tName of container t1Name) to (tFolder as alias) with using originals
end tell 

--tell application "Finder" to set myFiles to every file of folder tFolder
--repeat with aFile in myFiles
--	 open aFile
--	 set file_name	to name of aFile
--	 set file_name	to  aFile as alias as string
--end repeat

end run
