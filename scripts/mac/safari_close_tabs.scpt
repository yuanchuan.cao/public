on run argv
set tab_name to (item 1 of argv) as string
tell application "safari"
activate
 close (every tab of window 1 whose URL contains tab_name)
end tell
end run
