#!/bin/sh

unameOut="$(uname -s)"
case "${unameOut}" in
    Linux*)     machine=linux;;
    Darwin*)    machine=mac;;
    CYGWIN*)    machine=cygwin;;
    MINGW*)     machine=mingw;;
    *)          machine="UNKNOWN:${unameOut}"
esac
alias if_linux='if [ "$machine" = "linux" ];then '
alias if_mac='if [ "$machine" = "mac" ];then '
alias if_mac_linux='if [ "$machine" = "mac" ] || [ "$machine" = "linux" ];then '
alias if_install='if [ "$install" = "yes" ];then '
abs_path() 
{
    if_mac
	pushd . > /dev/null;
	if [ -d "$1" ];
	then cd "$1";
	     dirs -l +0;
	else cd "`dirname \"$1\"`";
	     cur_dir=`dirs -l +0`;
	     if [ "$cur_dir" == "/" ]; then
		 echo "$cur_dir`basename \"$1\"`";
	     else
		 echo "$cur_dir/`basename \"$1\"`";
	     fi;
	fi;
	popd > /dev/null;
fi
    if_linux
    readlink -f $1
    fi
}

base_name() 
{
    if_mac_linux
    basename $1
    fi
}

dir_name()
{
    if_mac_linux
    dirname $1
    fi
}

get_env() {
   if_mac_linux
     home_dir=$(abs_path ~)
     script_dir=$(dir_name $(abs_path $0))
     zsh_path=$home_dir/.zshrc
     bash_path=$home_dir/.bashrc
     file_dir=$(abs_path $script_dir/$1)
     current_dir=$(pwd)
     alias sga="sh $script_dir/sga.sh"
     config_path=$script_dir/../installation/system/$machine/config/software.config
     git --version 2>&1 >/dev/null 
     if_git_installed=$?
     if [ $if_git_installed -eq 0 ]; then
	 git_root_dir=`git rev-parse --show-toplevel 2>&1`
	 if [ -z "$git_root_dir" ] || [ -z "${git_root_dir##*not a git repository*}" ];then
	     git_root_dir=$current_dir
	     if_git_dir=0
	 else
	     if_git_dir=1
	 fi
	 if_git_istalled=1
     else
	 if_get_installed=0
     fi
fi

}

print_var() {
    eval "echo $1 is ${!1}"
}

pause_for_key () {
    read -p "Press any key to continue "
}

open_file () {
    if_mac
    osascript -e "tell application \"$1\" to open \"$2\""
    fi
}

quit_app () {
    if_mac    
    osascript -e "quit app \"$1\""
    fi
}

run_tool () {
    if_mac
    osascript $script_dir/mac/$1.scpt $2 $3 $4 $5 $6 $7 $8 $9 $10 $11
    fi
}

install_simple () {
    if_linux
    sudo apt install -y $1
fi
    if_mac
    brew install $1
fi
}

escape_special_str () {
    echo $1 | sed 's+"+\\"+g'
}

blank_to_path () {
    echo $1 | sed 's+\ +\\\ +g'
}

config_get () {
    eval $1=$(sga -m0 -c "^$1" $config_path \
		  | sga -m1 -M 1 -b "$1")
}

insert_or_replace_line () {
    if [ -f "$3" ];then
	local cmd="sh $script_dir/sga.sh -m 0 -c "\"$1\"" $3"
	tmp_found=$(eval $cmd)
	if [ -z "$tmp_found" ]; then
	    echo $2 >> $3
	else
	    local rep_str=$(escape_special_str "$2")
	    cmd="sh $script_dir/sga.sh -m 100 -c "\"$1\"" -r "\"$rep_str\"" $3"
	    eval $cmd
	fi
    else
	echo "$2" >> $3
    fi
}

