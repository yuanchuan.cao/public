#!/bin/sh
script_relative_dir=./
source $(dirname $0)/${script_relative_dir}function.sh
get_env ${script_relative_dir}


clear_scrn(){
    clear && printf '\e[3J'
}

tgt_dir="$home_dir/Downloads/tawawa/"


if [ -z "$1" ] && [ -z "$2" ]
then
    echo "No argument supplied"
    album_name="tawawa"
    img_dir=$tgt_dir$album_name"/"
else if [ -z "$2" ]
     then
	 album_name=$1
	 img_dir=$tgt_dir$album_name"/"
     else
	 album_name=$2
	 img_dir=$tgt_dir$album_name"/"
	 osascript $script_dir/mac/export_album.scpt "$tgt_dir" $1 $2 
     fi
fi


cd $img_dir
while true
do
    for filename in *; do
	clear_scrn
#	sh /Users/yuanchuan/Code/public/scripts/imgcat.sh "$filename"
	kitty +kitten icat "$filename"
	sleep 5
    done
done

