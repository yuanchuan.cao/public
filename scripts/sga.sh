#!/bin/sh
unameOut="$(uname -s)"
case "${unameOut}" in
    Linux*)     machine=linux;;
    Darwin*)    machine=mac;;
    CYGWIN*)    machine=cygwin;;
    MINGW*)     machine=mingw;;
    *)          machine="UNKNOWN:${unameOut}"
esac

alias if_linux='if [ "$machine" = "linux" ];then '
alias if_mac='if [ "$machine" = "mac" ];then '
alias if_mac_linux='if [ "$machine" = "mac" ] || [ "$machine" = "linux" ];then '

if_mac   # avoid being contaminated by gnu in brew install
    alias sed="/usr/bin/sed"
    alias grep="/usr/bin/grep"
    alias awk="/usr/bin/awk"
    alias tr="/usr/bin/tr"
fi

# https://stackoverflow.com/questions/16483119/an-example-of-how-to-use-getopts-in-bash
usage() { echo "usage:" && grep " .*)\ #" $0;
	  exit 0; }
[ $# -eq 0 ] && usage

while getopts ":h:M:b:e:m:c:d:r:o:n:" arg; do
    case $arg in
       m) # mode, details see below 
	    mode=${OPTARG}
	    ;;
       M) # max number of match, default unlimited
	    max_count=${OPTARG}
	    ;;
       b) # begin_ptn
	    begin_ptn=${OPTARG}
	    ;;
       e) # end_ptn
	    end_ptn=${OPTARG}
	    ;;
       c) # contains
	   contain_ptn=${OPTARG}
	   ;;
       d) # delimiter
	   delim=${OPTARG}
	   ;;
       r) # replace to
	   rep=${OPTARG}
	   ;;
       o) # concatenate multiple lines into one by "-d"(' ')
	   one_line=1
	   ;;
       n) # the nth field to be printed, default 2
	   nth_field=${OPTARG}
	   ;;
       h | *) # Display help.
	    usage
	    exit 0
	    ;;
    esac
done

# -m mode) # ----------------mode spec----------------------

case $mode in
    404) # version | of sed grep & awk 
	if_mac
	  sed_out=$(printf "\t" | sed "s+\t+gnu+g")
	  if [ "$sed_out" != "gnu" ];then
	      echo "BSD sed"
	  else
	      echo "GNU sed"
	  fi
	  grep --version
	  awk -version
        fi
        exit
	;;
    *)
    ;;
esac

shift $((OPTIND-1))

if [ $# = 0 ];then
#    read file < "${1-/dev/stdin}"
    #    file=$(tee)
    file=$(</dev/stdin)
    if_file=0
else
    if_file=1
    file=$1
fi


if [ -z "$max_count" ]; then
    max_count=0;
else
    max_arg=-m\ $max_count
fi

if [ -z "$delim" ]; then
    delim=0;
    sed_delim="+"
else
    sed_delim=$delim
    awk_delim_arg=-F$delim
fi


if [ -z "$one_line" ]; then
    one_line=0;
fi

if [ -z "$nth_field" ]; then
    nth_field=2;
fi


#if [ $one_line = 1 ]; then
# | tr '\n' ','
#fi

#echo mode is $mode
#echo file is $file
#echo max cases is $max_count
#echo delim is $delim
#echo contains: $contain_ptn
#echo one_line_arg is $one_line_arg

if_step2=0
do_file() {
    if [ -z "$file" ];then
	return
    fi
    # step2 first process
    if [ $if_file = 1 ];then
	file=$("$@" $file)
    else
	file=$(echo "$file" | "$@")	    
    fi
    if [ $if_step2 != 2 ];then
	echo "$file"
    fi
}


case $mode in
    0) # match lines | contain "-c"
	if_mac_linux
	    do_file grep $max_arg "$contain_ptn"
	fi
	;;
    1) # match fields | after field "-b", delim by "-d"(blank), "-n"(2)
	if_step2=2
	if_mac_linux
	    do_file grep -o $max_arg "$begin_ptn.*"
	    echo "$file" | awk $awk_delim_arg "\$1 == \"$begin_ptn\"{print \$${nth_field}}"
        fi
	;;
    2) # match pieces | between first match[exc] "-b" and first match[exc] "-e" 
	if_step2=2
	if_mac_linux
	do_file sed -e "s$sed_delim.*${begin_ptn}$sed_delim$sed_delim"
	echo "$file" | sed -e "s$sed_delim${end_ptn}.*$sed_delim$sed_delim"
	fi
	;;
    3) # tail of line | after last match[exc] "-e"
	if_mac_linux
      	do_file sed -e "s$sed_delim\(.*\)${end_ptn}$sed_delim$sed_delim"
	fi
	;;

    4) # head of line | before first match[exc] "-b"
	if_mac_linux
	do_file sed -e "s$sed_delim${begin_ptn}.*$sed_delim$sed_delim"
	fi
	;;
    100) # replace lines | contains "-c" to "-r", delim "-d"
	if_mac
           sed -i '' -e "s$sed_delim.*$contain_ptn.*$sed_delim$rep${sed_delim}g" $file
	fi
	if_linux
           sed -i "s$sed_delim.*$contain_ptn.*$sed_delim$rep${sed_delim}g" $file
	fi
	;;
    101) # replace pieces | "-c" to "-r", delim "-d"
	if [ $if_file = 1 ];then
	  if_mac
           sed -i '' -e "s$sed_delim$contain_ptn$sed_delim$rep${sed_delim}g" $file
          fi
          if_linux
           sed -i "s$sed_delim$contain_ptn$sed_delim$rep${sed_delim}g" $file
          fi
	else
            echo $file | sed -e "s$sed_delim$contain_ptn$sed_delim$rep${sed_delim}g" 
        fi
 	;;
    200) # file name without extension
	if_mac_linux
	    do_file sed -e 's/\.[^.]*$//'
	fi
	;;
    201) # file extension
	if_mac_linux
            do_file sed -e "s$sed_delim\(.*\)\.$sed_delim$sed_delim"
	fi
	;;
    *)
	;;
esac
