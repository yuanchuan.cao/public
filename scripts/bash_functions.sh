#!/bin/bash
zsh_path=~/.zshrc
bash_path=~/.bashrc



get_machine () {
    local unameOut="$(uname -s)"
    case "${unameOut}" in
	Linux*)     machine=linux;;
	Darwin*)    machine=mac;;
	CYGWIN*)    machine=cygwin;;
	MINGW*)     machine=mingw;;
	*)          machine="UNKNOWN:${unameOut}"
    esac
}

get_machine
config_path=$script_dir/../installation/system/$machine/config/software.config



print_var() {
    eval echo "$1 is $$$1"
}


pause_for_key () {
    read -p "Press any key to continue "
}


alias sga="sh $script_dir/sga.sh"		  
alias if_linux='if [ "$machine" = "linux" ];then '
alias if_mac='if [ "$machine" = "mac" ];then '
alias if_install='if [ "$install" = "yes" ];then '

if_mac
home_dir=$(grealpath ~)
fi
if_linux
home_dir=$(realpath ~)
fi

mac_open_file () {
    if_mac
    osascript -e "tell application \"$1\" to open \"$2\""
fi
}

mac_quit_app () {
    if_mac    
    osascript -e "quit app \"$1\""
fi
}

mac_run_tool () {
    if_mac
    osascript $script_dir/mac/$1.scpt $2 $3 $4 $5 $6 $7 $8 $9 $10 $11
fi
}


install_simple () {
    if_linux
    sudo apt install -y $1
fi
    if_mac
    brew install $1
fi
}

escape_special_str () {
    echo $1 | sed 's+"+\\"+g'
}

blank_to_path () {
    echo $1 | sed 's+\ +\\\ +g'
}

config_get () {
    eval $1=$(sga -m0 -c "^$1" $config_path \
		  | sga -m1 -M 1 -b "$1")
}

insert_or_replace_line () {
    if [ -f "$3" ];then
	local cmd="sh $script_dir/sga.sh -m 0 -c "\"$1\"" $3"
	tmp_found=$(eval $cmd)
	if [ -z "$tmp_found" ]; then
	    echo $2 >> $3
	else
	    local rep_str=$(escape_special_str "$2")
	    cmd="sh $script_dir/sga.sh -m 100 -c "\"$1\"" -r "\"$rep_str\"" $3"
	    eval $cmd
	fi
    else
	echo "$2" >> $3
    fi
}
