#!/bin/sh
script_relative_dir=./
. $(dirname $0)/${script_relative_dir}function.sh
get_env ${script_relative_dir}
# (bashInit "./")

if_mac
    pprof_cmd=pprof
    image_viewer=open
    callgrind_viewer=qcachegrind
fi
if_linux
    pprof_cmd=google-pprof
    image_viewer=ristretto
    callgrind_viewer=kcachegrind
fi

script_path=$script_dir/projectile.sh
git_path=$script_dir/git.sh
alias g='sh $git_path'

create_gitignore () {
    cp $script_dir/projectile/.gitignore_template $add_location/.gitignore
    cp $script_dir/projectile/.projectile_template $add_location/.projectile
}

create_project_dir () {
    read -p "project name: " proj_name
    if [ -z "$proj_name" ]; then
	echo "please give project name, exit..."
	exit
    else
	mkdir $proj_name
    fi
    proj_path=$current_dir/$proj_name
}
get_git_provider () {
    read -p "git provider name: [gitlab]: " git_prefix
    git_prefix=${git_prefix:-gitlab}
    echo "git provider: $git_prefix"
}

get_maintainer () {
    read -p "maintainer name: [shima_rin]: " maintainer
    maintainer=${maintainer:-shima_rin}
    echo "maintainer: $maintainer"
}

get_committer () {
    read -p "committer name: [shima_rin]: " committer
    committer=${committer:-shima_rin}
    echo "committer: $committer"
}

get_remote_info () {
    get_git_provider
    get_maintainer
}

clone_repo (){
    git clone git@${git_prefix}_${committer}:${maintainer}/${proj_name}.git \
	$add_location
}

add_committer () {
    get_committer
    add_location=$proj_path/$committer
    echo "add committer: $committer"
    clone_repo
}

add_default_files () {
    mkdir -p $add_location/src $add_location/include
    create_gitignore
    read -p "if project with C Makefile: [y]: " yn
    yn=${yn:-y}
    if [ "$yn" = "y" ]; then
	cp $script_dir/projectile/Makefile $add_location/
	cp -rf $script_dir/projectile/kbuild $add_location/
    fi
}

if_continue () {
    printf "%s " $comment
    read yn
}

create_success () {
    ls -al $proj_path
    echo "project $proj_name successfully created: "
    echo "go into maintaner dir, g b-a wip, g push, g b main"
    echo "go into worker dir, g pull, g b wip"
}

case $1 in
    new)
	read -p "if project with remote git: [y]: " yn
	yn=${yn:-y}
	if [ "$yn" = "y" ]; then
	    sh $script_path new-git
	    exit;
	fi
	create_project_dir
	add_location=$proj_path
	add_default_files
	create_success
	;;
    new-git)
	echo "\t0) go to git_site: init with readme.md" 
	echo "\t1) project info/members/invite group/xx"
	echo "\t2) settings/repo/proctec branch/ toggle on maintainer force push"
	echo "\t3) optional change avatar"

	create_project_dir
	get_remote_info

	read -p "if project multi committer: [y]: " yn
	yn=${yn:-y}
	if [ "$yn" != "y" ]; then
	    add_location=$proj_path
	    git clone git@${git_prefix}_${maintainer}:${maintainer}/\
		${proj_name}.git
	    add_default_files
	    cd $add_location && sh $git_path push "init from local"
	    create_success
	    exit;
	fi
	echo "previous get info, now add maintainer again"
	add_committer
	add_location=$proj_path/$committer	
	add_default_files
	cd $add_location && sh $git_path push "init from local"
	
	comment="if continue add committer... ? [y/n]"
	if_continue
	while [ "$yn" = "y" ]
	do
	    add_committer
	    if_continue
	done
	create_success
	;;
    clone)
	create_project_dir
	get_remote_info
	read -p "if project multi committer: [y]: " yn
	yn=${yn:-y}
	if [ "$yn" != "y" ]; then
	    git clone git@${git_prefix}_${maintainer}:${maintainer}/\
		${proj_name}.git
	    create_success
	    exit;
	fi

	add_committer
	comment="if continue add committer... ? [y/n]"
	if_continue
	while [ "$yn" = "y" ]
	do
	    add_committer
	    if_continue
	done
	create_success
	;;
    clear-projectile)
	rm -rf ~/.emacs.d/projectile-bookmarks.eld
	;;
    info)
	g info
	;;
    r)
	if [ -z $T ];then
	    cd $git_root_dir && make
	else
	    T=$T para=$para sh $script_path b
	    T=$T para=$para sh $script_path run
	fi
	;;
    b)
	cd $git_root_dir && make build T=$T
	;;
    run)
	cd $git_root_dir && make run T=$T para=$para
	;;
    all)
	T=$T para=$para sh $script_path build
	T=$T para=$para sh $script_path profiler
	T=$T para=$para sh $script_path valgrind
	;;
    profile)
	cd $git_root_dir/build/test
	env HEAPPROFILE=memory.prof CPUPROFILE=cpu.prof \
	    ./test_$T $para || true
	$pprof_cmd ./test_$T  cpu.prof --web > /dev/null 2>&1 &
	$pprof_cmd ./test_$T  memory.prof.*.heap --web > /dev/null 2>&1 &
	;;
    profile-local)			#profiler in code block
	cd $git_root_dir/build/test
	$pprof_cmd ./test_$T  cpu.prof --web > /dev/null 2>&1 &
	$pprof_cmd ./test_$T  memory.prof.*.heap --web > /dev/null 2>&1 &
	;;
    valgrind)
	if [ "$os" = "Darwin" ];then
	    leaks $(pgrep -f $git_root_dir/build/test/test_$T |head -n 1)  
	    leaks $(pgrep -f $git_root_dir/build/test/test_$T |sed -n '2p')
	    echo "-------------------------------------------------"
	    echo "using leaks you have to add getchar() to keep  the program running"
	    echo "using another shell to make leaks to read the leaks report"
	fi

	if [ "$os" = "Linux" ];then
	    cd $git_root_dir/build/test
	    echo "valgrind memory leak/miss/error check:"
	    valgrind --tool=memcheck --leak-check=full \
	             --show-leak-kinds=all --verbose \
		     --track-origins=yes \
		     --log-file=test_$T.valgrind\
		     ./test_$T $para >/dev/null 2>&1

	    echo "valgrind call graph profiler"
	    valgrind --tool=callgrind \
		     --callgrind-out-file=test_$T.callgrind\
		     ./test_$T $para >/dev/null 2>&1

	    gprof2dot -f callgrind test_$T.callgrind\
		| dot -Tpng -o callgrind.png\
		&& $image_viewer callgrind.png >/dev/null 2>&1 &
	    
	    $callgrind_viewer test_$T.callgrind >/dev/null 2>&1 &
	    echo "------------------------------------------"
	    echo "-----valgrind cache miss & branch prediction------"
	    echo "----------------------------------------"
	    valgrind --tool=cachegrind \
		     ./test_$T $para 
	    echo "------------------------------------------"
	    echo "------- valgrind memcheck  file----------"
	    echo "----------------------------------------"
	    cat test_$T.valgrind
	fi
	;;
    asm)
	if [ -z $F ]; then
	    objdump -S --no-show-raw-insn --no-leading-headers  \
		    $FLAG -d -t  $git_root_dir/build/$T
	else
	    objdump -S --no-show-raw-insn --no-leading-headers  \
		    $FLAG -d --disassemble-functions=_$F   $git_root_dir/build/$T
	fi
	;;
    asm-cmp)
	objdump -S --no-show-raw-insn --no-leading-headers \
		$FLAG -d --disassemble-functions="_$F1,_$F2" \
		$git_root_dir/build/$T
	;;
    macro)
	gcc -E $2 | less
	;;
    md)
	if [ -z "$2" ];then
	    if [ "$if_git_dir" -eq "1" ];then
		cd $git_root_dir
		MDV_THEME=845.69 mdv README.md | less
	    fi
	else
	    md_dir=$(gsed 's/(//g;s/)//g' ~/.emacs.d/projectile-bookmarks.eld | tr ' ' '\n' | grep -m1 "$2" | tr -d '"')
	    md_path=${md_dir}README.md
	    eval MDV_THEME=845.69 mdv $md_path | less
	fi
	;;
    wiki-of)
	if [ -z "$2" ];then
	    if [ "$if_git_dir" -eq "1" ];then
		cd $git_root_dir/wiki
		eval MDV_THEME=845.69 mdv index.md | less
	    fi
	else
	    md_dir=$(gsed 's/(//g;s/)//g' ~/.emacs.d/projectile-bookmarks.eld | tr ' ' '\n' | grep -m1 "$2" | tr -d '"')
	    if [ -z "$3" ];then
		md_path=${md_dir}wiki/index.md
	    else
		md_path=${md_dir}wiki/$3.md
	    fi
	    eval MDV_THEME=845.69 mdv $md_path | less
	fi
	;;
    wiki)
	if [ "$if_git_dir" -eq "1" ];then
	    cd $git_root_dir/wiki
	else
	    exit
	fi
	if [ -z "$2" ];then
	    eval MDV_THEME=845.69 mdv index.md | less
	else
	    eval MDV_THEME=845.69 mdv $2.md | less	    
	fi
	;;
    gdb)
	cd $git_root_dir/build/test
	gdb -i=mi -ex=r --args ./test_$T $para
	;;
    lldb)
	cd $git_root_dir/build/test
	lldb -o run -- ./test_$T $para
	;;
    clear)
	cd $git_root_dir
	find . -type f -name "*~" -exec rm {} +
	find . -type f -name "#*#" -exec rm {} +
	;;
    clean)
	cd $git_root_dir && make clean
	;;
    cstandard)
	sh $script_dir/projectile.sh wiki-of /helium3/miyu standard
	;;
    chelp)
	sh $script_dir/projectile.sh wiki-of /helium3/miyu manual
	;;
    mdhelp)
	sh $script_dir/projectile.sh wiki-of /helium3/miyu md
	;;
    --test)
	;;
    *)
	echo "Usage: $0 <arg1,[arg2]...> #comment"
	printf "%80s\n" |tr " " "-"
	printf "\t%-20s| %-20s| %-20s| %-20s\n" "new #new project" "clone" "info" "h"
	;;
esac
