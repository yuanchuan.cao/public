trade_server=172.19.91.205

case $1 in
    -ag)
	sudo mysql  -e "SELECT * FROM futures5.ag2106 ORDER BY i DESC LIMIT 1" ;;
    -au)
	sudo mysql  -e "SELECT * FROM futures5.au2106 ORDER BY i DESC LIMIT 1" ;;
    -update)
	sh ~/Code/katou/katou_core/src/test/sync_db.sh ;;
    -ag5)
	mysql --user=root -e "SELECT * FROM futures5.ag2106 ORDER BY i DESC LIMIT 1" ;;
    -au5)
	mysql --user=root -e "SELECT * FROM futures5.au2106 ORDER BY i DESC LIMIT 1" ;;
    -tau)
	mysql --user=root -e "SELECT * FROM traders.trader_au2012 ORDER BY i DESC LIMIT $2" ;;
    -tag)
	mysql --user=root -e "SELECT * FROM traders.trader_ag2012 ORDER BY i DESC LIMIT $2" ;;
    -du)
	mysql --user=root -e "SELECT table_name, (data_length+index_length)/power(1024,2) tablesize_mb
	      		  	FROM information_schema.tables
				WHERE table_schema='futures5' ;";;
    -csv)
	mysql --user=root -e "SELECT  year,month,day,hour,min,sec,milli,lastprice,askprice1,askvolume1,bidprice1,bidvolume1,volume,turnover,oi
	      		        INTO OUTFILE '/tmp/$2_$3'
				FIELDS TERMINATED BY ','
				LINES TERMINATED BY '\n'
	      		  	FROM futures.$2
	      		        WHERE month = '$3'
				order by i 
				;"
	cp /tmp/$2_$3 /root/Data/
	rm /tmp/$2_$3;;
    -csv5)
	mysql --user=root -e "SELECT  year,month,day,hour,min,sec,milli,lastprice,volume,turnover,oi,askprice1,askvolume1,bidprice1,bidvolume1,askprice2,askvolume2,bidprice2,bidvolume2,askprice3,askvolume3,bidprice3,bidvolume3,askprice4,askvolume4,bidprice4,bidvolume4,askprice5,askvolume5,bidprice5,bidvolume5
	      		        INTO OUTFILE '/tmp/$2_$3_5'
				FIELDS TERMINATED BY ','
				LINES TERMINATED BY '\n'
	      		  	FROM futures5.$2
	      		        WHERE month = '$3'
				order by i 
				;"
	cp /tmp/$2_$3_5 /root/Data/
	rm /tmp/$2_$3_5;;
    -latest)
	mysql --user=root -e "SELECT  year,month,day,hour,min,sec,milli,lastprice,askprice1,askvolume1,bidprice1,bidvolume1,volume,turnover,oi
	      		        INTO OUTFILE '/tmp/$2_$3'
				FIELDS TERMINATED BY ','
				LINES TERMINATED BY '\n'
	      		  	FROM futures.$2
	      		        WHERE month = '$3'
				order by i 
				;"
	cp /tmp/$2_$3 /root/Data/
	rm /tmp/$2_$3

	LINE_COUNT=`sed -n '$=' ~/Data/$2_$3`
	case $# in
	    4)
		LINE_WANTED=600000
		;;
	    5)
		LINE_WANTED=$4
		;;
	esac
	if [ $LINE_COUNT -gt $LINE_WANTED ]; then
	    echo $LINE_COUNT is greater than $LINE_WANTED
	    tail -$LINE_WANTED ~/Data/$2_$3 > ~/Data/nowdata
	else
	    LINE_COUNT=`expr $LINE_WANTED - $LINE_COUNT`
	    LAST_MONTH=`expr $3 - 1`
	    echo $LINE_COUNT from ~/Data/$2_$LAST_MONTH
	    tail -$LINE_COUNT ~/Data/$2_$LAST_MONTH > ~/Data/nowdata
	    cat ~/Data/$2_$3 >> ~/Data/nowdata
	fi

	LAST_DAY=`expr $4 - 1`
	scp root@$trade_server:main/krr/alllog-$3_$LAST_DAY ~/Data/nowlog
	scp root@$trade_server:main/krr/alllog-$3_$4 ~/Data/tmpfile
	cat ~/Data/tmpfile >> ~/Data/nowlog
	rm ~/Data/tmpfile
	tail ~/Data/nowdata
	tail ~/Data/nowlog
	;;
    -test)
	
	;;
    try)
	;;
esac
