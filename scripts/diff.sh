#! /bin/bash
line=$(cmp "$1" "$2" | awk '{print $NF}')

if [ -z "$n" ];then
    n=2
fi

total=$((n * 2))

rm -rf $1.tmp $2.tmp
for i in $(seq 1 1 $total)
do
    sed -n "$((line - n + i)) p " $1 >> $1.tmp
    echo "-------------------" >> $1.tmp
    sed -n "$((line - n + i)) p " $2 >> $2.tmp
    echo "-------------------" >> $2.tmp
done

icdiff -W $1.tmp $2.tmp

rm -rf $1.tmp $2.tmp
