(defun debian-startup ()
  (split-window-vertically)
  (other-window 1)
  (eshell)
  (other-window 1)  
  )



(defun entertainment-startup (mainscript subscript width height x y)
  (split-window-horizontally)
  (find-file mainscript)
  (other-window 1)
  (find-file subscript)
  (split-window-vertically)
  (other-window -1)
  (when window-system (progn
			(set-frame-size (selected-frame) width height)
			(set-frame-position (selected-frame) x y))
	)
  )


(defun entertainment-startup1 (music pict timer scale mainscript subscript)
  (split-window-horizontally)
  (other-window 1)
  (split-window-horizontally)
  (split-window-vertically)
;;  (mu4e)
  (find-file mainscript)
  (other-window 1)
;;  (emms-play-playlist music)
;;  (emms-playlist-mode-go)
  (find-file mainscript)
  (other-window 1)
  (split-window-vertically)
;;  (w3m)
;;  (w3m-gohome)
  (find-file mainscript)
  (sleep-for 0.3)
  (other-window 1)
  (find-file mainscript)
  (sleep-for 1)
;;  (my-image-dir-view pict timer scale)
;;  (sleep-for 1)
  (other-window 1)
  (find-file mainscript)
  (split-window-horizontally)
  (other-window 1)
  (eshell)  
  (split-window-vertically)
  (other-window 1)
  (find-file subscript)
  (other-window -1)
  (other-window -1)
  (other-window -1)
;;  (tawawa timer scale)
  (my-image-mode)
  (sleep-for 0.3)
  (other-window 1)
  ;;  (toggle-frame-fullscreen)
  (toggle-frame-maximized)  
;;  (setq default-frame-alist '((left . 1) (width . 1) (fullscreen . fullheight)))
  )

;; (entertainment-startup)
(defun my-image-test ()
  (setq tawawa "~/Code/picts/src/tawawa/227.png")
  (setq oral "~/Pictures/19397061.gif")
  (setq tawawadir "~/Code/picts/src/tawawa/")
  (my-image-view tawawa (current-buffer) 0.2)
  (my-image-view oral (current-buffer))
  (my-image-dir-view tawawadir 5 0.4)
  )

(defun my-image-dir-view (dir timer scale)
;;  (interactive
;;   "DDirectory to open: \n"      ;;sRename buffer %s to: "
   (let* ((filelist (directory-files-recursively dir ""))
	  (buffer (current-buffer))
	  )
     (my-image-filelist-view filelist 0 buffer timer scale)
     )
;;   )
   )

(defun my-image-filelist-view (filelist index buffer timer scale)
  (let ((filename (nth index filelist))
	(count (length filelist))
	)
    (with-current-buffer buffer
      (erase-buffer)
      (my-image-view filename buffer scale)
      )
    (run-with-timer timer
		    nil
		    #'my-image-filelist-view
		    filelist
		    (% (1+ index) count)
		    buffer
		    timer
		    scale)
    )
  )

(defun my-image-view (filename buffer scale)
;;  (interactive
;;   "bFile to open: \n"      ;;sRename buffer %s to: "
   (let* ((img (create-image
		filename
		'imagemagick
		nil
		:scale scale))
;;	  (height (window-pixel-height))
;;	 (width (window-pixel-width))
;;	 (size (image-size img))
	 )
;;(setf (image-property img :scale) 0.5)
;;(image-type-available-p 'imagemagick)
;;(add-to-list 'exec-path "/usr/local/Cellar/imagemagick/7.0.10")
     (insert-image img)
     (if (image-multi-frame-p img)
	 (my-image-animate img buffer nil t))
     )
   ;;   )
   )



(defun my-image-animate (image buffer &optional index limit)
  "Start animating IMAGE.
Animation occurs by destructively altering the IMAGE spec list.

With optional INDEX, begin animating from that animation frame.
LIMIT specifies how long to animate the image.  If omitted or
nil, play the animation until the end.  If t, loop forever.  If a
number, play until that number of seconds has elapsed."
  (let ((animation (image-multi-frame-p image))
	timer)
    (when animation
      (if (setq timer (image-animate-timer image))
	  (cancel-timer timer))
      (plist-put (cdr image) :animate-buffer buffer)
      (run-with-timer 0.2 nil #'my-image-animate-timeout
		      image (or index 0) (car animation)
		      0 limit (+ (float-time) 0.2)))))


(defun my-image-animate-timeout (image n count time-elapsed limit target-time)
  "Display animation frame N of IMAGE.
N=0 refers to the initial animation frame.
COUNT is the total number of frames in the animation.
TIME-ELAPSED is the total time that has elapsed since
`image-animate-start' was called.
LIMIT determines when to stop.  If t, loop forever.  If nil, stop
 after displaying the last animation frame.  Otherwise, stop
 after LIMIT seconds have elapsed.
The minimum delay between successive frames is `image-minimum-frame-delay'.

If the image has a non-nil :speed property, it acts as a multiplier
for the animation speed.  A negative value means to animate in reverse."
  (when (and t ;; buffer-live-p (plist-get (cdr image) :animate-buffer))
             ;; Delayed more than two seconds more than expected.
	     (or (<= (- (float-time) target-time) 10)
		 (progn
		   (message "Stopping animation; animation possibly too big")
		   nil)))
    (image-show-frame image n t)
    (let* ((speed (image-animate-get-speed image))
	   (time (float-time))
	   (animation (image-multi-frame-p image))
	   ;; Subtract off the time we took to load the image from the
	   ;; stated delay time.
	   (delay (max (+ (* (or (cdr animation) image-default-frame-delay)
			     (/ 1.0 (abs speed)))
			  time (- (float-time)))
		       image-minimum-frame-delay))
	   done)
      (setq n (if (< speed 0)
		  (1- n)
		(1+ n)))
      (if limit
	  (cond ((>= n count) (setq n 0))
		((< n 0) (setq n (1- count))))
	(and (or (>= n count) (< n 0)) (setq done t)))
      (setq time-elapsed (+ delay time-elapsed))
      (if (numberp limit)
	  (setq done (>= time-elapsed limit)))
      (unless done
	(run-with-timer delay nil #'my-image-animate-timeout
			image n count time-elapsed limit
                        (+ (float-time) delay))))))



(make-variable-buffer-local
 (defvar my-image-filelist nil
   "Number of my-images inserted into the current buffer."))


(defun tawawa (timer scale)
  (interactive)
  (switch-to-buffer "*MY-IMAGE*")
  (my-image-dir-view  "~/Code/picts/src/tawawa" timer scale)
  )

(defun sex ()
  (interactive)
  (switch-to-buffer "*MY-IMAGE*")
  (my-image-dir-view  "~/Pictures/sex" timer 1)
  )

;;;###autoload
(define-minor-mode my-image-mode
  "Get your my-images in the right places."
  :lighter " my-image"
  :keymap (let ((map (make-sparse-keymap)))
            (define-key map (kbd "t") 'tawawa)
            (define-key map (kbd "s") 'sex)
            (define-key map (kbd "q") 'kill-this-buffer)
            map))

;;;###autoload
;; (add-hook 'text-mode-hook 'my-image-mode)

(provide 'my-image-mode)


(defun kindle-push ()
  (interactive)
  (let* (
	 (base (file-name-base (buffer-file-name (current-buffer))))
	 (dir default-directory)
	 (html (concat dir base ".html"))
	 (mobi (concat "/run/media/stan/Kindle/documents/diy/" base ".azw3"))
	 )
    (format "%s - %s " html mobi)
    (org-html-export-to-html)
    (call-process "ebook-convert" nil 0 nil
		  html
		  mobi
		  "--tags"
		  "diy"
		  "--title"
		  base
		  )
    )
  )

(defun kindle-view ()
  (interactive)
  (let* (
	 (base (file-name-base (buffer-file-name (current-buffer))))
	 (dir default-directory)
	 (html (concat "file://" dir base ".html"))
	 )
    (org-html-export-to-html)
    (w3m-browse-url html)
    (call-process "rm" nil 0 nil  html)
    )
  )
