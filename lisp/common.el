;; (byte-compile-file "common.el")

(setq common-el-file load-file-name)
(if
    (eq system-type 'darwin)
    (setq machine-type "mac")
  )


(defun package_setup()
  (require 'package)
  (setq package-archives '(("gnu"   . "http://elpa.emacs-china.org/gnu/")
			   ("melpa" . "http://elpa.emacs-china.org/melpa/")
			   ("gnu-cn"   . "http://elpa.zilongshanren.com/gnu/")
			   ("melpa-cn" . "http://elpa.zilongshanren.com/melpa/")
			   ))
  (package-initialize)
  )

;; w3m
(defun w3m-link-numbering (&rest args)                                          
  "Make overlays that display link numbers."                                    
  (when w3m-link-numbering-mode                                                 
    (save-excursion                                                             
      (goto-char (point-min))                                                   
      (let ((i 0)                                                               
            overlay num)                                                        
        (catch 'already-numbered                                                
          (while (w3m-goto-next-anchor)                                         
            ;; 这里判断是否是一个锚链接                                         
            (when (w3m-anchor)                                                  
              (when (get-char-property (point) 'w3m-link-numbering-overlay)     
                (throw 'already-numbered nil))                                  
              (setq overlay (make-overlay (point) (1+ (point)))                 
                    num (format "[%d]" (incf i)))                               
              (w3m-static-if (featurep 'xemacs)                                 
			     (progn                                                        
			       (overlay-put overlay 'before-string num)                    
			       (set-glyph-face (extent-begin-glyph overlay)                
					       'w3m-link-numbering))                       
			     (w3m-add-face-property 0 (length num) 'w3m-link-numbering num)  
			     (overlay-put overlay 'before-string num)                        
			     (overlay-put overlay 'evaporate t))                             
              (overlay-put overlay 'w3m-link-numbering-overlay i))))))))        



(defun w3m_setup()
  (setq w3m-command "/usr/bin/w3m")
  (require 'w3m)
  (require 'w3m-lnum)
  (setq w3m-command-arguments '("-cookie" "-F"))
  (setq w3m-use-cookies t)

  (define-key w3m-mode-map (kbd "s") 'w3m-history)
  (define-key w3m-mode-map (kbd "f") 'w3m-lnum-goto)

  (setq w3m-default-display-inline-image t) 
  (setq w3m-default-toggle-inline-images t)
  (setq w3m-bookmark-file "~/Code/public/org/emacs/w3m_bookmark.html")
  )

(defun my-mu4e-set-account ()
  "Set the account for composing a message."
  (if mu4e-compose-parent-message
      (let ((mail (cdr (car (mu4e-message-field mu4e-compose-parent-message :to)))))
	(if (member mail mu4e-user-mail-address-list)
	    (progn
              (setq user-mail-address mail)
              (setq smtpmail-smtp-user mail)
	      )
	  (progn
	    (setq user-mail-address "cyc@illya.co")
            (setq smtpmail-smtp-user "cyc@illya.co")
	    )))
    (helm :sources
	  `((name . "Select account: ")
            (candidates . mu4e-user-mail-address-list)
            (action . (lambda (candidate) (progn
					    (setq user-mail-address candidate)
					    (setq smtpmail-smtp-user candidate))))))))

(defun mu4e_setup()
  (require 'mu4e)                      ; load mu4e
  ;; Use mu4e as default mail agent
  (setq mail-user-agent 'mu4e-user-agent)
  (setq mu4e-maildir "~/Maildir")         ; NOTE: should not be symbolic link
  (setq mu4e-get-mail-command "offlineimap")
  (setq mu4e-update-interval 60)

  (setq mu4e-maildir-shortcuts
	'(("/Personal/INBOX"   . ?p)
	  ("/Work/INBOX" . ?w)
	  ("/Register/INBOX" . ?r)
	  ("/INBOX" . ?i)
	  ))

  ;; folder for sent messages
  (setq mu4e-sent-folder   "/Sent")
  (setq mu4e-drafts-folder "/Drafts")
  (setq mu4e-trash-folder  "/Trash")
  (setq mu4e-trash-folder  "/INBOX")
  (setq mu4e-view-show-images t)
  (setq mu4e-compose-signature "Sent from my emacs.")

  (require 'smtpmail)
  (autoload 'async-smtpmail-send-it "~/.emacs.d/emacs-async/smtpmail-async.el" nil t)
  (setq send-mail-function    'async-smtpmail-send-it
	smtpmail-smtp-server  "smtp.zoho.com.cn"
	smtpmail-stream-type  'ssl
	smtpmail-smtp-service 465)

  (setq user-full-name "Yuanchuan Cao")          ; FIXME: add your info here
  (setq user-mail-address ".co")   ; FIXME: add your info here
  (setq smtpmail-smtp-user ".co")  ; FIXME: add your gmail addr here

  (add-to-list 'mu4e-user-mail-address-list ".co")
  (add-to-list 'mu4e-user-mail-address-list ".co")
  (add-to-list 'mu4e-user-mail-address-list ".co")

  (add-hook 'mu4e-compose-pre-hook 'my-mu4e-set-account)
  (global-set-key (kbd "C-c e m") 'mu4e)
  (setq message-kill-buffer-on-exit t)
  )

(defun org_setup()
  (require 'org-contacts)
  (setq mu4e-org-contacts-file  "~/Code/org/emacs/contact.org")		     
  (add-to-list 'mu4e-headers-actions				     
	       '("org-contact-add" . mu4e-action-add-org-contact) t)   
  (add-to-list 'mu4e-view-actions					     
	       '("org-contact-add" . mu4e-action-add-org-contact) t)   

  (use-package org
    :ensure org-plus-contrib
    :bind ("C-c d" . org-decrypt-entry)
    :custom
    (org-modules '(org-crypt))
    (org-tags-exclude-from-inheritance (quote ("crypt")))
    (org-startup-with-inline-images t)
    (org-tag-alist '(
		     ("crypt" . ?C)
		     ))
    (org-agenda-include-diary t)
    )

  (use-package org-crypt
    :ensure nil
    :after org
    :custom (org-crypt-key "")
    (auto-save-default nil))

  (use-package org-journal
    :after org
    :custom
    (org-journal-enable-agenda-integration)
    (org-journal-update-org-agenda-files)
    (org-journal-dir "~/Code/org/emacs/journal/")
    (org-journal-file-format "%Y-%m-%d.org")
    (org-journal-date-format "%e %b %Y (%A)")
    (org-journal-time-format ""))

  (add-to-list 'auto-mode-alist
	       '("\\.vue\\'" .(lambda ()
				(web-mode)
				(auto-complete-mode))))
  (org-crypt-use-before-save-magic)
  )

(setq org-agenda-file-regexp "`[^.].*.org'|[0-9]+")                            

(defun emms_setup()
  ;; git clone git://git.sv.gnu.org/emms.git
  ;;  (add-to-list 'load-path "~/.emacs.d/lisp/emms/lisp/")
  ;;  (require 'emms)
  
  (require 'emms-setup)
  ;;  (require 'emms-bilibili)  
  ;;  (setq emms-bilibili-mid 1324556)  
  (autoload 'helm-emms "helm-emms" nil t)
  ;;  (autoload 'emms-smart-browse "emms-browser.el" "Browse with EMMS" t)
  ;;  (require 'emms-info-mediainfo)
  ;;  (add-to-list 'emms-info-functions #'emms-info-mediainfo)
  (emms-all)
  (emms-default-players)
  ;;  (emms-standard)
  ;;  (require 'emms-simple-player-mpv)
  ;;  (setq emms-player 'emms-simple-player-mpv)

  (emms-cache-disable)
  (setq emms-repeat-playlist t
	emms-source-file-default-directory "~/Music/"
	emms-lyrics-dir "~/Music/"
	emms-lyrics-coding-system nil
	emms-playlist-buffer-name "*EMMS*")

  ;;  (require 'emms-info-libtag) ;;; load functions that will talk to emms-print-metadata which in turn talks to libtag and gets metadata
  ;;  (setq emms-info-functions '(emms-info-libtag)) ;;; make sure libtag is the only thing delivering metadata
;;; below is a nice key command for toggling the music browser
  ;;  (autoload 'emms-smart-browse "emms-browser.el" "Browse with EMMS" t)
  ;;  (global-set-key [(f7)] 'emms-smart-browse)

  (global-set-key (kbd "C-c e d") 'helm-emms)
  (global-set-key (kbd "C-c e g") 'emms-play-playlist)
  (global-set-key (kbd "C-c e v") 'emms-playlist-mode-go)
  (global-set-key (kbd "C-c e x") 'emms-start)
  (global-set-key (kbd "C-c e SPC") 'emms-pause)
  (global-set-key (kbd "C-c e s") 'emms-stop)
  (global-set-key (kbd "C-c e n") 'emms-next)
  (global-set-key (kbd "C-c e p") 'emms-previous)
  (global-set-key (kbd "C-c e f") 'emms-seek-forward)
  (global-set-key (kbd "C-c e b") 'emms-seek-backward)
  (global-set-key (kbd "C-c e +") 'volume-set)
  (require 'volume)
  ;;  (require 'emms-player-simple-mpv)
  (add-to-list 'emms-player-mpv-parameters "--geometry=0%:100%")
  (add-to-list 'emms-player-mpv-parameters "--volume=50")
  (add-to-list 'emms-player-mpv-parameters "--no-focus-on-open")

  ;;  (provide 'emms-player-mpd)
  ;;  (setq emms-volume-change-function emms-
  
  ;; C-c e d to go to a file C-k to kill it
  ;; C-x r b to gote fav.m3u C-y and save it
  ;; C-c e g to play fave.m3u

  )

(defun fav()
  (interactive)
  (emms-play-playlist "~/Music/fav.m3u")
  ;;  (emms-playlist-mode-go)
  )

(defun porn()
  (interactive)
  (emms-play-directory "~/Movies/porn")
  ;;  (emms-playlist-mode-go)
  )

(defun porn1()
  (interactive)
  (emms-play-directory "~/Downloads/tmp")
  ;;  (emms-playlist-mode-go)
  )


(defun display_setup()
  ;;  (eshell-git-prompt-use-theme 'default)
  (require 'cnfonts)
  (cnfonts-enable)
  (load-theme 'zenburn t)
  (set-frame-parameter (selected-frame) 'alpha '(95 . 85))
  (set-face-attribute 'region nil :background "#dcf252")
  )


(defun copy-from-osx ()
  (shell-command-to-string "pbpaste"))

(defun paste-to-osx (text &optional push)
  (let ((process-connection-type nil))
    (let ((proc (start-process "pbcopy" "*Messages*" "pbcopy")))
      (process-send-string proc text)
      (process-send-eof proc))))


(defun misc_setup()
  (add-to-list 'exec-path "/usr/local/bin")
  (if
      (eq system-type 'darwin)
      (progn
	(setq interprogram-cut-function 'paste-to-osx)
	(setq interprogram-paste-function 'copy-from-osx)
	)
    )
  (setq x-select-enable-clipboard t)
  (setq tramp-default-method "ssh") ;; this avoids asking y/n when ssh accept host public key
  (setq image-cache-eviction-delay 1)
  (setq bookmark-default-file
	(format "%s../installation/software/emacs/%s/default.bmk"
		(file-name-directory common-el-file)
		machine-type))

  (use-package auto-complete
    :ensure t
    :init
    :config
    (progn
      (require 'auto-complete-config)
      (setq-default ac-sources '
		    (ac-source-abbrev
		     ac-source-dictionary
		     ac-source-words-in-same-mode-buffers
		     ac-source-filename
		     ac-source-symbols
		     ac-source-functions
		     ac-source-features
		     ac-user-dictionary)
		    )
      (add-to-list 'ac-modes '(org-mode text-mode))
      (global-auto-complete-mode t)
      ))


  (set-keyboard-coding-system 'utf-8)

  (set-clipboard-coding-system 'utf-8)

  (set-terminal-coding-system 'utf-8)

  (set-buffer-file-coding-system 'utf-8)

  (set-selection-coding-system 'utf-8)

  (modify-coding-system-alist 'process "*" 'utf-8)
  (setq default-process-coding-system '(utf-8 . utf-8))

  (setq-default pathname-coding-system 'utf-8)

  (load "~/Code/public/lisp/linux-kernel-coding-style")
  (require 'linux-kernel-coding-style)

  
  (setq-default
   ;;   tab-width 8
   whitespace-line-column 70
   whitespace-style       '(face lines-tail))
  
  (add-hook 'prog-mode-hook #'whitespace-mode)

  ;;  (add-hook 'c-mode-hook (lambda () (setq comment-start "/*"
  ;;					  comment-end   "*/")))
  )

(defun helm_setup()
  (require 'helm)
  (require 'helm-config)

  ;; The default "C-x c" is quite close to "C-x C-c", which quits Emacs.
  ;; Changed to "C-c h". Note: We must set "C-c h" globally, because we
  ;; cannot change `helm-command-prefix-key' once `helm-config' is loaded.




  (global-set-key (kbd "C-c h") 'helm-command-prefix)
  (global-unset-key (kbd "C-x c"))
  (global-set-key (kbd "C-x b") 'helm-mini)
  (global-set-key (kbd "C-x C-f") 'helm-find-files)
  (global-set-key (kbd "M-x") 'helm-M-x)
  (global-set-key (kbd "M-y") 'helm-show-kill-ring)
  (global-set-key (kbd "C-c h o") 'helm-occur)

  (define-key helm-map (kbd "<tab>") 'helm-execute-persistent-action) ; rebind tab to run persistent action
  (define-key helm-map (kbd "C-i") 'helm-execute-persistent-action) ; make TAB work in terminal
  (define-key helm-map (kbd "C-z")  'helm-select-action) ; list actions using C-z

  (setq helm-M-x-fuzzy-match t
	helm-semantic-fuzzy-match t
	helm-imenu-fuzzy-match    t
	helm-buffers-fuzzy-matching t
	helm-recentf-fuzzy-match    t)      

  (setq semantic-mode 1)

  (add-to-list 'helm-sources-using-default-as-input 'helm-source-man-pages)

  (setq helm-split-window-in-side-p           t ; open helm buffer inside current window, not occupy whole other window
	helm-move-to-line-cycle-in-source     t ; move to end or beginning of source when reaching top or bottom of source.
	helm-ff-search-library-in-sexp        t ; search for library in `require' and `declare-function' sexp.
	helm-scroll-amount                    8 ; scroll 8 lines other window using M-<next>/M-<prior>
	helm-ff-file-name-history-use-recentf t
	helm-echo-input-in-header-line t)

  (setq helm-autoresize-max-height 0)
  (setq helm-autoresize-min-height 20)
  (helm-autoresize-mode 1)

  (helm-mode 1)

  ;; helm-proj
  (projectile-global-mode)
  (setq projectile-git-submodule-command nil)
  (setq projectile-project-root-files #'(".projectile"))	
  (setq projectile-enable-caching t)
  (setq projectile-indexing-method 'native)


  (setq projectile-completion-system 'helm)
  (helm-projectile-on)
  (setq projectile-switch-project-action 'helm-projectile)
  (add-to-list 'projectile-globally-ignored-directories "backup")
  (add-to-list 'projectile-globally-ignored-directories "production")
  (define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)
  ;;
  (require 'helm-eshell)

  (add-hook 'eshell-mode-hook
	    #'(lambda ()
		(define-key eshell-mode-map (kbd "C-c C-l")  'helm-eshell-history)))

  ;;  (define-key shell-mode-map (kbd "C-c C-l") 'helm-comint-input-ring)
  (put 'projectile-project-test-cmd 'safe-local-variable 'stringp)
  (put 'projectile-project-run-cmd 'safe-local-variable 'stringp)
  (put 'projectile-project-compilation-cmd 'safe-local-variable 'stringp)
  )

(defun switch-to-previous-window ()
  (interactive)
  (other-window -1))

(defun delete-process-at-point()
  (interactive)
  (let ((process (get-process "compilation")))
    (if process
	(delete-process process))))

(defun close-all-buffers ()
  (interactive)
  (mapc 'kill-buffer (buffer-list)))

(defun insert-myReplace()
  (interactive)
  (insert "//(myReplace \"x\" \"y\" \"\" \"\")")
  )

(defun key_setup()
  (global-set-key (kbd "C-'") 'insert-myReplace)
  (global-set-key (kbd "C-x p") 'switch-to-previous-window)
  (global-set-key  (kbd "C-x SPC") 'set-mark-command)
  (global-set-key (kbd "C-x >") 'end-of-buffer)
  (global-set-key (kbd "C-x <") 'beginning-of-buffer)
  (global-set-key (kbd "C-c 1") 'select-window-1)
  (global-set-key (kbd "C-c 2") 'select-window-2)
  (global-set-key (kbd "C-c 3") 'select-window-3)
  (global-set-key (kbd "C-c 4") 'select-window-4)
  (global-set-key (kbd "C-c 5") 'select-window-5)
  (global-set-key (kbd "C-x C-k") 'delete-process-at-point)
  (global-set-key (kbd "C-c p p") 'helm-projectile-switch-project)
  (global-set-key (kbd "C-c p f") 'helm-projectile-find-file)
  (global-set-key (kbd "C-c p c") 'projectile-compile-project)
  (global-set-key (kbd "C-c p t") 'projectile-test-project)
  (global-set-key (kbd "C-c p s g") 'projectile-find-regexp)
  (global-set-key (kbd "C-c a") 'auto-complete-mode)
  ;;  (global-set-key (kbd "C-c e e") 'my-encrypt-buffer)
  ;;  (global-set-key (kbd "C-c e E") 'my-decrypt-buffer)
  (global-unset-key (kbd "C-z"))
  )

(defun load_setup()
  (load "~/Code/public/lisp/c_macro") ;; here i load this lisp file during init of emac
  (load "~/Code/public/lisp/entertainment") ;; here i load this lisp file during init of em;;
  )

(defun eshell/clear ()
  (interactive)
  (let ((eshell-buffer-maximum-lines 0)) (eshell-truncate-buffer)))


(defvar all-gud-modes
  '(gud-mode comint-mode gdb-locals-mode gdb-frames-mode  gdb-breakpoints-mode)
  "A list of modes when using gdb")
(defun kill-all-gud-buffers ()
  "Kill all gud buffers including Debugger, Locals, Frames, Breakpoints.
Do this after `q` in Debugger buffer."
  (interactive)
  (save-excursion
    (let ((count 0))
      (dolist (buffer (buffer-list))
        (set-buffer buffer)
        (when (member major-mode all-gud-modes)
          (setq count (1+ count))
          (kill-buffer buffer)
          (delete-other-windows))) ;; fix the remaining two windows issue
      (message "Killed %i buffer(s)." count))))







(defun set-gdb-layout(&optional c-buffer)
  (if (not c-buffer)
      (setq c-buffer (window-buffer (selected-window)))) ;; save current buffer

  ;; from http://stackoverflow.com/q/39762833/846686
  (set-window-dedicated-p (selected-window) nil) ;; unset dedicate state if needed
  (switch-to-buffer gud-comint-buffer)
  (delete-other-windows) ;; clean all

  (let* (
         (w-gdb (selected-window)) ;; left top
         (w-source (split-window w-gdb nil 'right)) ;; right bottom
         (w-dissamble (split-window w-gdb nil 'below))
         (w-locals (split-window w-source nil 'below)) ;; right middle bottom
         (w-breakpoints (split-window w-locals nil 'above)) ;; right top
         (w-stacks (split-window w-breakpoints nil 'below)) ;; right top
         (w-io (split-window w-dissamble (floor(* 0.7 (window-body-height)))
                             'below)) ;; left bottom
         )
    (set-window-buffer w-io (gdb-get-buffer-create 'gdb-inferior-io))
    (set-window-dedicated-p w-io t)
    (set-window-buffer w-breakpoints (gdb-get-buffer-create 'gdb-breakpoints-buffer))
    (set-window-dedicated-p w-breakpoints t)
    (set-window-buffer w-locals (gdb-get-buffer-create 'gdb-locals-buffer))
    (set-window-dedicated-p w-locals t)
    (set-window-buffer w-dissamble (gdb-get-buffer-create 'gdb-disassembly-buffer))
    (set-window-dedicated-p w-dissamble t)

    (set-window-buffer w-stacks (gdb-get-buffer-create 'gdb-stack-buffer))
    (set-window-dedicated-p w-stacks t)

    (set-window-buffer w-gdb gud-comint-buffer)

    (select-window w-gdb)
    (set-window-buffer w-source c-buffer)
    ))
(defadvice gdb (around args activate)
  "Change the way to gdb works."
  (setq global-config-editing (current-window-configuration)) ;; to restore: (set-window-configuration c-editing)
  (let (
        (c-buffer (window-buffer (selected-window))) ;; save current buffer
        )
    ad-do-it
    (set-gdb-layout c-buffer))
  )
(defadvice gdb-reset (around args activate)
  "Change the way to gdb exit."
  ad-do-it
  (set-window-configuration global-config-editing))

(defun gdb_setup()
  (setq gdb-many-windows nil)
  )

(if (version< "27.0" emacs-version)
    (set-fontset-font
     "fontset-default" 'unicode "Apple Color Emoji" nil 'prepend)
  (set-fontset-font
   t 'symbol (font-spec :family "Apple Color Emoji") nil 'prepend))


(defun my-watch-in-mpv (url)
  (async-shell-command (format "mpv -v %S" url)))


(defun telegram_setup()
  (setq telega-proxies
	(list
	 '(:server "127.0.0.1" :port 7891 :enable t
                   :type (:@type "proxyTypeSocks5" :username "" :password ""))))

  (setq telega-use-images nil)

  (define-key global-map (kbd "C-c t") telega-prefix-map)
  (setq telega-chat-input-markups '(nil "markdown2" "markdown1"))
  )

(defun common_setup()
  (setq inhibit-startup-message t)
  (line-number-mode t)
  (column-number-mode t)
  (global-linum-mode t)
  (show-paren-mode t)
  (menu-bar-mode -1)
  (toggle-scroll-bar -1)
  (tool-bar-mode -1)
  (display-time-mode 1)
  (defalias 'yes-or-no-p 'y-or-n-p)
  (setq frame-title-format
	'(buffer-file-name "%f"
			   (dired-directory dired-directory "%b")))
  
  (package_setup)
  (window-numbering-mode)
  
  ;;  (w3m_setup)
  ;;  (mu4e_setup)
  ;;  (org_setup)
  (emms_setup)
  (display_setup)
  (helm_setup)
  (load_setup)
  (misc_setup)
  (key_setup)
  (telegram_setup)
  )
;; suggested .emacs:

;; (load "~/Code/lisp/common") ;; here i load this lisp file during init of emacs, so
;; (common_setup)
;;(entertainment-startup "~/Music/fav.m3u"
;;		       "~/Code/kato_ult/strategy.cpp"
;;		       "~/Code/kato_ult/databox.h"
;;		       )

