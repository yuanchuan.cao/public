default:
	make clean
clean:
	find . -name "*~" -type f -delete

clear-projectile-project:
	rm -rf ~/.emacs.d/projectile-bookmarks.eld

#projectile_root_file:   (setq projectile-project-root-files #'(".projectile"))	
#clear_projectile_cache [C-c p i]:  (projectile-invalidate-cache nil)
#recompile_lisp:  (byte-recompile-directory "./lisp" 0)
#git: type g
