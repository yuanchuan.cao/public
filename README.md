# public

## deploy debian machine
### initial setup
#### on local machine
```bash
 curl -fsSL https://gitlab.com/shima_rin/public/-/raw/main/gen_push_key.sh | sh
# then wait for the instruction on remote machine
```

#### on remote machine
```bash
su root
wget https://gitlab.com/shima_rin/public/-/raw/main/deploy_debian.sh
sh deploy_debian.sh
exit
su user
curl -fsSL https://gitlab.com/shima_rin/public/-/raw/main/deploy_debian/deploy_debian_sub.sh | sh
```
