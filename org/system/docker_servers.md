# docker

## install docker
```bash
	curl -fsSL http://mirrors.ustc.edu.cn/docker-ce/linux/debian/gpg | sudo apt-key add -
	echo "deb [arch=amd64] http://mirrors.ustc.edu.cn/docker-ce/linux/debian buster stable" >> /etc/apt/sources.list.d/docker.list
	sudo apt update && apt install docker-ce
	sudo systemctl start docker
```

## install gitlab

### pull image
```bash
sudo docker pull gitlab/gitlab-ce
sudo docker run -d  -p 4433:443 -p 8888:80 -p 2222:22 --name gitlab --restart always -v /home/gitlab/config:/etc/gitlab -v /home/gitlab/logs:/var/log/gitlab -v /home/gitlab/data:/var/opt/gitlab gitlab/gitlab-ce
```

### set config

tail /home/gitlab/config/gitlab.rb: 

```
puma['worker_processes'] = 0	       
sidekiq['max_concurrency'] = 10	     
prometheus_monitoring['enable'] = false
# 配置ssh协议所使用的访问地址和端口
gitlab_rails['gitlab_ssh_host'] = '139.9.45.251'
gitlab_rails['gitlab_shell_ssh_port'] = 2222 # 此端口是run时22端口映射的222端口
```
sudo docker restart gitlab

### reset password
sudo docker ps 
docker exec -it f0b035c718a0 /bin/bash

```shell
gitlab-rails console -e production
user = User.where(username: 'root').first
user.password = 'xxxx'
user.save!
```

## install webdav
```bash
sudo docker pull bytemark/webdav
sudo docker run --name webdav --restart always -v /home/webdav:/var/lib/dav -e AUTH_TYPE=Basic -e USERNAME=x_x -e PASSWORD=pass --publish 6666:80 -d bytemark/webdav
```


